<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $kaki;
    public $keahlian; 

    public function setNama($nama){
        echo 'nama: '. $this->nama = $nama;
    }
    public function setKaki($kaki){
        echo 'jumlah kaki: ' . $this->kaki = $kaki;
    }
    public function setKeahlian($keahlian){
        echo 'keahlian: ' . $this->keahlian = $keahlian;
    }

    public function atraksi(){
        echo $this->nama . " -- " . $this->keahlian;
    }
}

trait Fight{
    use Hewan;
    public $atkPower;
    public $defPower;

    public function setAtk($atkPower){
        echo 'attack power: ' . $this->atkPower = $atkPower;
    }
    public function setDef($defPower){
        echo 'defense power: ' . $this->defPower = $defPower;
    }
   
    public function serang(){
        echo $this->nama . ' menyerang '. $this->nama . ".";

        
        function diserang(){
            echo $this->nama . ' diserang.';
            echo '<br>';

            function getdarah($darah){
            echo 'darah ' . $this->nama . '= ' . $this->darah - ($this->atkPower / $this->defPower);
            }
        }

    }
    
}

class Elang{
    use Hewan;  
    use Fight;

    public function getInfoHewan(){
        echo $this->setNama("Elang") . "<br>";
        echo $this->setKaki(2) . "<br>";
        echo $this->setKeahlian("terbang tinggi") . "<br>";
        echo $this->setAtk(10) . "<br>";
        echo $this->setDef(5);
    }
}

class Harimau{
    use Hewan;  
    use Fight;

    public function getInfoHewan(){
        echo $this->setNama("Harimau") . "<br>";
        echo $this->setKaki(4) . "<br>";
        echo $this->setKeahlian("lari cepat") . "<br>";
        echo $this->setAtk(7) . "<br>";
        echo $this->setDef(8);
    }
}


$hewan1 = new Elang;
$hewan1->getInfoHewan();
echo "<br>" . "<br>";
$hewan2 = new Harimau;
$hewan2->getInfoHewan();
echo "<br>" . "<br>";
echo $hewan1->atraksi();
echo "<br>";
echo $hewan2->atraksi();
echo "<br>" . "<br>";
echo $hewan1->serang();

?>